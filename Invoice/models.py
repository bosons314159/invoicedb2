from django.db import models
# Create your models here.

class Transaction(models.Model):
    product = models.CharField(max_length=255)
    quantity = models.IntegerField(default=1)
    price = models.FloatField(default=0.0)
    line_total = models.FloatField(default=0.0)
    transaction_id = models.AutoField(null=False,primary_key=True, unique=True)

class Invoice(models.Model):
    customer = models.CharField(max_length=255)
    total_quantity = models.IntegerField(default=1)
    total_amount = models.FloatField(default=0.0)
    invoice_id = models.AutoField(null=False,primary_key=True,unique=True)
    date = models.DateTimeField(auto_now_add=True)
    transaction_id = models.ManyToManyField(Transaction)
