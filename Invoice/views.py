import logging
from django.core import serializers
from rest_framework.exceptions import APIException
from rest_framework.decorators import api_view
from django.shortcuts import render
from django.http import HttpResponse,HttpResponseNotFound,Http404
from rest_framework.views import APIView
from rest_framework.response import Response
import datetime
from django.template import engines, Template,Context
import json
from .models import Transaction, Invoice

logger = logging.getLogger('invoicedb')
hdlr = logging.FileHandler('invoicedb.log')
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
hdlr.setFormatter(formatter)
logger.addHandler(hdlr)
logger.setLevel(logging.INFO)
django_engine = engines['django']

@api_view(["GET"])
def index(request):
    return HttpResponse("Hello, World")

# Create your views here.
@api_view(["GET","PUT","DELETE"])
def detail(request, Invoice_id):
    if request.method == "GET":
        try:
            i = Invoice.objects.get(invoice_id=Invoice_id)
        except:
            raise Http404("Invoice does not exist")
        return render(request, 'invoice/detail.html',{'invoice': i})
    elif request.method == "PUT":
        return HttpResponse("PUT METHOD")
    elif request.method == "DELETE":
        return HttpResponse("DELETE METHOD")
    elif request.method == "POST":
        return HttpResponse("POST METHOD")


@api_view(["GET","POST"])
def lists(request):
    if request.method == "GET":
        try:
            i = Invoice.objects.all()
        except:
            raise Http404("There are no invoices")
        return render(request, django_engine.from_string("{{ i }}"))
    elif request.method == "POST":
        try:
            total_quant = 0
            total_amount = 0.0
            trans= request.data['transactions']
            i = Invoice(customer=request.data["customer"])
            trans = []
            for x in trans:
                line_t = float(x['quantity'])*float(x['price'])
                total_quant= total_quant+ x['quantity']
                total_amount = total_amount + line_t
                t = Transaction()
                t.product=x['product']
                t.quantity=x['quantity']
                t.price=x['price']
                t.line_total=line_t
                t.save()
                trans.append(t)
            i['total_quantity']=total_quant
            i['total_amount']=total_amount
            i.save()
            for k in trans:
                i.transaction_id.add(k)
            i.save()
            return HttpResponse("you have sinned")
        except Exception as e:
            logger.error(e)
            return HttpResponse("Hello, World")
