from django.urls import include, path
from . import views

urlpatterns = [
        path('',views.lists,name="lists"),
        path('<int:invoice_id>/',views.detail,name="detail")
]
